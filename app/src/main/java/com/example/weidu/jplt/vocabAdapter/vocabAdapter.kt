package com.example.weidu.jplt.vocabAdapter

import android.content.Context
import android.graphics.Color
import android.support.constraint.R.id.parent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.weidu.jplt.R
import kotlinx.android.synthetic.main.vocab_row.view.*
import java.lang.Double.min
import kotlin.math.min


/**
 * Created by wei.du on 15/12/2017.
 */
class vocabAdapter(Context:Context, var word: ArrayList<String>, var kanji: ArrayList<String>, var meaning: ArrayList<String>, var type: ArrayList<String>, var times: ArrayList<String>):RecyclerView.Adapter<vocabAdapter.vocabHolder>(){
    var endcolor = 0x64e3e7
    var context = Context

    override fun onBindViewHolder(holder: vocabHolder?, position: Int) {
        holder!!.vocab.setText(word[position])
        holder.kanji.setText(kanji[position])
        holder.type.setText(type[position])
        holder.number.setText(position.toString())
        val color : Int = 0xFF-(0xFF/20*min(15,times[position].toInt()))

        val colorstring = (color.shl(8)+color.shl(16)+0xFF).toString(16)
        //Log.e("color", colorstring)
        holder.itemView.setBackgroundColor(Color.parseColor("#"+colorstring))
        holder.itemView.setOnClickListener {
            // display a toast with person name on item click
            Toast.makeText(context,meaning[position],Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return word.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): vocabHolder{
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.vocab_row,parent,false)
        return vocabHolder(view)
    }


    class vocabHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var vocab : TextView = itemView.findViewById(R.id.vocab)
        var kanji : TextView = itemView.findViewById(R.id.kanji)
        var type : TextView = itemView.findViewById(R.id.type)
        var number : TextView = itemView.findViewById(R.id.number)
    }
}
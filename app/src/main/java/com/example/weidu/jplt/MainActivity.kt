package com.example.weidu.jplt

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.weidu.jplt.vocabAdapter.vocabAdapter
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.util.*


class MainActivity : AppCompatActivity() {

    //Arraylist of data
    var quit = false
    var kanji:ArrayList<String> = ArrayList()
    var vocab:ArrayList<String> = ArrayList()
    var meaning:ArrayList<String> = ArrayList()
    var type:ArrayList<String> = ArrayList()
    var times:ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView

        var linearLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = linearLayoutManager


        val obj = JSONObject(loadJSONFromAsset())
        val vocabArray = obj.getJSONArray("vocab")
        for(i in 0 until 20){
            var random_index =(Math.random()*vocabArray.length()).toInt()
            var singlevocab = vocabArray.getJSONObject(random_index)
            //Log.e("JSON Parser","vocab "+ singlevocab.getString("word"));
            vocab.add(singlevocab.getString("word"))
            kanji.add(singlevocab.getString("kanji"))
            meaning.add(singlevocab.getString("meaning"))
            type.add(singlevocab.getString("type"))
            times.add(singlevocab.getString("times"))
        }

        var vocabAdapter = vocabAdapter(applicationContext,vocab,kanji,meaning,type,times)
        recyclerView.adapter = vocabAdapter

        val NextButton = findViewById<View>(R.id.button2) as Button
        NextButton.setOnClickListener {
            vocab.clear()
            kanji.clear()
            meaning.clear()
            type.clear()
            times.clear()

            for(i in 0 until 20){
                var random_index =(Math.random()*vocabArray.length()).toInt()
                var singlevocab = vocabArray.getJSONObject(random_index)
                //Log.e("JSON Parser","vocab "+ singlevocab.getString("word"));
                vocab.add(singlevocab.getString("word"))
                kanji.add(singlevocab.getString("kanji"))
                meaning.add(singlevocab.getString("meaning"))
                type.add(singlevocab.getString("type"))
                times.add(singlevocab.getString("times"))
            }

            var vocabAdapter = vocabAdapter(applicationContext,vocab,kanji,meaning,type,times)
            recyclerView.adapter = vocabAdapter
        }

    }

    // Disable back key
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        val timer = Timer()
        val t = object : TimerTask() {
            override fun run() {
                quit = false
            }
        }

        when(keyCode){
            KeyEvent.KEYCODE_BACK ->{
                if(true == quit){
                    finish()
                }else{
                    Toast.makeText(this.applicationContext,"press back again to quit JPLT", Toast.LENGTH_SHORT).show()
                    quit = true
                    timer.schedule(t, 2000)
                }
                return true
            }

        }
        return super.onKeyDown(keyCode, event)
    }


    fun loadJSONFromAsset(): String? {
        var json : String? = null
        try {
            val `is` = assets.open("vocab.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, charset("UTF-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }
}
